function [result]=partialsum(fn,n,x)
result = zeros(x-n+1,1);
total = 0;
for a=n:x
    total=total+fn(a);
    result(a-n+1)=total;
end
plot(result);